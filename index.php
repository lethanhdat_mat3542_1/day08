<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Programming Day02</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js">
    </script>
    <style>
        .bold {
            font-weight: bold;
        }

        .danger {
            color: red;
        }

        body {
            display: flex;
            justify-content: center;
            margin-top: 40px;
        }

        .wrapper {
            border: 1.5px solid #4475a2;
            padding: 30px 60px;
        }

        label {
            background-color: rgb(85, 159, 39);
            width: 150px;
            display: inline-block;
            line-height: 30px;
            padding-left: 10px;
            color: white;
            border: 1.5px solid #4475a2;
        }

        .input-box {
            margin-bottom: 10px;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .input-radio {
            margin-bottom: 10px;
        }

        .button-box {
            margin-top: 30px;
            display: flex;
            justify-content: center;
        }

        .input-username {
            border: 1px solid #4475a2;
            width: 300px;

        }


        .button {
            background-color: rgb(85, 159, 39);
            ;
            color: white;
            padding: 12px 38px;
            border: 1.5px solid #4475a2;
            border-radius: 7px;
        }

        .khoa-classname {
            width: 300px;
        }

        .khoa-select {
            display: flex;
            justify-content: space-between;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        .them {
            margin-top: 0px !important;
        }

        .button-add {
            padding: 0px 10px !important;
            background-color: blue;
        }

        .button-add2 {
            padding: 0px 10px !important;
            background-color: green;
            margin-left: 10px;
            margin-right: 5px;
        }

        .hover:hover {
            cursor: pointer;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <form method="POST" enctype="multipart/form-data">
            <?php
            $khoa = $tukhoa = "";
            if (isset($_POST['timkiem'])) {
                if (!empty($_POST['tukhoa'])) {
                    $tukhoa = $_POST['tukhoa'];
                }
                if (!empty($_POST['khoa'])) {
                    $khoa = $_POST['khoa'];
                }
            }
            ?>
            <div class="input-box khoa-select">
                <label>Khoa </label>
                <select class="khoa-classname" id="khoa" name="khoa">
                    <?php
                    $arr_khoas = [
                        "" => "",
                        "MAT" => "Khoa học máy tính",
                        "KDL" => "Khoa học dữ liệu"
                    ];
                    foreach ($arr_khoas as $key => $arr_khoa) {
                        $selected = !empty($khoa) &&  $khoa === $key ? "selected=" . $key : "";
                        echo '<option ' . $selected . ' value="' . $key . '">' . $arr_khoa . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="input-box username-box">
                <label>Từ khóa </label>
                <input type="text" class="input-username" name="tukhoa" id="tukhoa" value="<?php echo $tukhoa; ?>">
            </div>
            <div class="d-flex justify-content-center">
                <div class="button-box ml-3">
                    <div class="button hover" onclick="handleOnclick()">
                        Xóa
                    </div>
                </div>
                <form method="POST" enctype="multipart/form-data">
                    <div class="button-box ml-3">
                        <button type="submit" name="timkiem" class="button hover">
                            Tìm kiếm
                        </button>
                    </div>
                </form>
            </div>
        </form>
        <div class="my-3 d-flex justify-content-between">
            <div class="pt-3">
                Số sinh viên tìm thấy: XXX
            </div>
            <div class="button-box them">
                <form method="POST" action="form.php" class="d-flex">
                    <button type="submit" class="button button-add" name="add">
                        Thêm
                    </button>
                </form>
            </div>
        </div>
        <?php
        $arr_names = [
            "Nguyễn Văn A" => "MAT",
            "Trần Thị B" => "MAT",
            "Nguyễn Hoàng C" => "KDL",
            "Đinh Quang D" => "KDL",
        ];
        ?>
        <table>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($arr_names as $name => $arr_name) {
                    $i++;
                    echo ' <tr>
                <td>' . $i . '</td>
                <td>' . $name . '</td>
                <td>' . $arr_khoas[$arr_name] . '</td>
                <td class="d-flex justify-content-center">
                    <div class="button-box them">
                        <button type="submit" class="button button-add2">
                            Xóa
                        </button>
                    </div>
                    <div class="button-box them">
                        <button type="submit" class="button button-add2">
                            Sửa
                        </button>
                    </div>
                </td>
            </tr>';
                }
                ?>
            </tbody>
        </table>
    </div>

    <script type="text/javascript">
        $(".date").datepicker({
            format: "<?php echo $date_format; ?>",
        });
    </script>
</body>
<script>
    const handleOnclick = () => {
        const tukhoa = document.getElementById("tukhoa")
        const khoa = document.getElementById("khoa")
        tukhoa.value = ""
        khoa.value = ""
        console.log({
            tukhoa,
            khoa
        });
    }
</script>

</html>